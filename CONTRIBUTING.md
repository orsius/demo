Bonjour,

Nous sommes ravis de constater votre enthousiasme à participer à notre projet. Voici les étapes à suivre pour contribuer :

1. Créez une issue
2. Effectuez un fork du projet
3. Apportez vos modifications
4. Testez vos changements :  
   4.1 En utilisant Gitpod (recommandé)  
   4.2 Ou via votre propre environnement de développement  
=> Que vous soyez en Gitpod ou sur votre propre environnement, pour tester, exécutez "make tests"
5. Soumettez une demande de fusion (merge request)
6. Patientez pendant la revue de code
