ansible-compat==3.0.1
ansible-lint==6.15.0
docker==6.1.0
molecule-plugins[docker]==23.4.0
molecule==5.0.0
urllib3==1.26.15
yamllint==1.31.0
